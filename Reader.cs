﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using AForge.Video;
using AForge.Video.DirectShow;

namespace HawkQR
{
    public partial class Reader : Form
    {
        private Home frm_parent;
        private Auxiliar aux;
        private HQRreader hqr_reader;
        private FilterInfoCollection webcam_list;
        private VideoCaptureDevice webcam_selected;
        private bool test_mode;
        private string id;

        public Reader(Home fp)
        {
            InitializeComponent();
            frm_parent = fp;
            aux = new Auxiliar();
            chkbx_passwd_CheckedChanged(null, null);
            lbl_source_refresh_Click(null, null);
        }

        //Establecer mensaje de estado
        private void SetStatus(string text)
        {
            lbl_status.Text = text;
        }

        //Obtener cámaras web y cargarlas
        private void lbl_source_refresh_Click(object sender, EventArgs e)
        {
            cmbbx_source.Items.Clear();
            webcam_list = new FilterInfoCollection(FilterCategory.VideoInputDevice);
            for (int w = 0; w < webcam_list.Count; w++)
            {
                cmbbx_source.Items.Add(webcam_list[w].Name);
            }
            if (webcam_list.Count == 0)
            {
                SetStatus("Error. No hay fuentes de video disponibles");
            }
            else
            {
                SetStatus("Listo para iniciar");
            }           
        }

        //Filtrar contraseña
        private void txtbx_passwd_TextChanged(object sender, EventArgs e)
        {
            txtbx_passwd.Text = aux.FilterString(txtbx_passwd.Text, 'p');
            if (txtbx_passwd.Text.Length > 0)
            {
                txtbx_passwd.SelectionStart = txtbx_passwd.Text.Length;
            }
        }

        //Mostrar/Ocultar contraseña
        private void chkbx_passwd_CheckedChanged(object sender, EventArgs e)
        {
            txtbx_passwd.UseSystemPasswordChar = !txtbx_passwd.UseSystemPasswordChar;
        }

        //Iniciar/Detener transimisión de video
        private void btn_process_Click(object sender, EventArgs e)
        {
            //Comprobar fuente de video seleccionada y longitud de contraseña válida
            if (cmbbx_source.SelectedIndex >= 0)
            {
                if (txtbx_passwd.Text.Length >= 9)
                {
                    if (btn_process.Text.Equals("Iniciar"))
                    {
                        SetStatus("Transmitiendo desde fuente de video..."); 
                        EnableControls(false);
                        hqr_reader = new HQRreader(txtbx_passwd.Text);
                        //Configurar e iniciar transmisión de video
                        test_mode = true;
                        webcam_selected = new VideoCaptureDevice(webcam_list[cmbbx_source.SelectedIndex].MonikerString);
                        webcam_selected.NewFrame += new NewFrameEventHandler(webcam_newframe);
                        webcam_selected.Start();
                        btn_process.Text = "Detener";
                    }
                    else if (btn_process.Text.Equals("Detener"))
                    {
                        EnableControls(true);
                        btn_process.Text = "Iniciar";
                        SetStatus("Listo para iniciar");
                        pctbx_webcam.Image = null;
                        webcam_selected.Stop();
                    }
                }
                else
                {
                    SetStatus("Error. La contraseña debe tener de 9-30 caracteres");
                }
            }
            else
            {
                SetStatus("Error. Selecciona una fuente de video");
            }
        }

        //Nuevo frame recibido de la fuente de video
        private Bitmap frame;
        private void webcam_newframe(object sender, NewFrameEventArgs eventArgs)
        {
            frame = (Bitmap)eventArgs.Frame.Clone();
            //Tomar el primer frame para ajustar el lector
            if (test_mode)
            {
                hqr_reader.Prepare(frame.Width, frame.Height);
                test_mode = false;
            }            
            else
            {
                //Analizar frames en busca de códigos HQR
                if (hqr_reader.Process(frame))
                {
                    frm_parent.sound_hqr.Play();
                    MessageBox.Show("ID: " + hqr_reader.id_decoded, "Hawk QR", 
                        MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
                //Mostrar cuadrado central del frame original
                frame = hqr_reader.frame_original_cropped;
            }
            pctbx_webcam.Image = frame;
        }

        //Activar o Desactivar los controles durante el proceso de generación
        private void EnableControls(bool status)
        {
            cmbbx_source.Enabled = status;
            txtbx_passwd.Enabled = status;
            chkbx_passwd.Enabled = status;
        }

        //Detener transmisión para Home
        public void StopStream()
        {
            if (btn_process.Text.Equals("Detener"))
            {
                webcam_selected.Stop();
            }
        }
    }
}
