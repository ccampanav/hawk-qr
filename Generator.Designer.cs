﻿namespace HawkQR
{
    partial class Generator
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Generator));
            this.lstbx_models_list = new System.Windows.Forms.ListBox();
            this.lbl_models_title = new System.Windows.Forms.Label();
            this.lbl_models_refresh = new System.Windows.Forms.Label();
            this.lbl_models_count = new System.Windows.Forms.Label();
            this.lbl_template = new System.Windows.Forms.Label();
            this.lbl_color = new System.Windows.Forms.Label();
            this.lbl_origin = new System.Windows.Forms.Label();
            this.pctbx_color = new System.Windows.Forms.PictureBox();
            this.pctbx_output = new System.Windows.Forms.PictureBox();
            this.lbl_passwd_title = new System.Windows.Forms.Label();
            this.txtbx_passwd = new System.Windows.Forms.TextBox();
            this.chkbx_passwd = new System.Windows.Forms.CheckBox();
            this.lbl_details_title = new System.Windows.Forms.Label();
            this.btn_generate = new System.Windows.Forms.Button();
            this.lbl_output_title = new System.Windows.Forms.Label();
            this.pctbx_status = new System.Windows.Forms.PictureBox();
            this.lbl_status = new System.Windows.Forms.Label();
            this.txtbx_id = new System.Windows.Forms.TextBox();
            this.lbl_id_title = new System.Windows.Forms.Label();
            this.lbl_output_time = new System.Windows.Forms.Label();
            this.lbl_output_result = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.pctbx_color)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pctbx_output)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pctbx_status)).BeginInit();
            this.SuspendLayout();
            // 
            // lstbx_models_list
            // 
            this.lstbx_models_list.BackColor = System.Drawing.Color.White;
            this.lstbx_models_list.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.lstbx_models_list.Cursor = System.Windows.Forms.Cursors.Hand;
            this.lstbx_models_list.ForeColor = System.Drawing.Color.DimGray;
            this.lstbx_models_list.FormattingEnabled = true;
            this.lstbx_models_list.ItemHeight = 21;
            this.lstbx_models_list.Location = new System.Drawing.Point(10, 50);
            this.lstbx_models_list.Name = "lstbx_models_list";
            this.lstbx_models_list.ScrollAlwaysVisible = true;
            this.lstbx_models_list.Size = new System.Drawing.Size(220, 315);
            this.lstbx_models_list.TabIndex = 0;
            this.lstbx_models_list.SelectedIndexChanged += new System.EventHandler(this.lstbx_models_list_SelectedIndexChanged);
            // 
            // lbl_models_title
            // 
            this.lbl_models_title.Font = new System.Drawing.Font("Segoe UI", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_models_title.Location = new System.Drawing.Point(70, 16);
            this.lbl_models_title.Name = "lbl_models_title";
            this.lbl_models_title.Size = new System.Drawing.Size(100, 26);
            this.lbl_models_title.TabIndex = 1;
            this.lbl_models_title.Text = "Modelos";
            this.lbl_models_title.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // lbl_models_refresh
            // 
            this.lbl_models_refresh.Cursor = System.Windows.Forms.Cursors.Hand;
            this.lbl_models_refresh.Font = new System.Drawing.Font("Segoe UI", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_models_refresh.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(227)))), ((int)(((byte)(181)))), ((int)(((byte)(106)))));
            this.lbl_models_refresh.Location = new System.Drawing.Point(10, 368);
            this.lbl_models_refresh.Name = "lbl_models_refresh";
            this.lbl_models_refresh.Size = new System.Drawing.Size(70, 20);
            this.lbl_models_refresh.TabIndex = 3;
            this.lbl_models_refresh.Text = "Refrescar";
            this.lbl_models_refresh.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.lbl_models_refresh.Click += new System.EventHandler(this.lbl_models_refresh_Click);
            // 
            // lbl_models_count
            // 
            this.lbl_models_count.Font = new System.Drawing.Font("Segoe UI", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_models_count.Location = new System.Drawing.Point(130, 368);
            this.lbl_models_count.Name = "lbl_models_count";
            this.lbl_models_count.Size = new System.Drawing.Size(100, 20);
            this.lbl_models_count.TabIndex = 4;
            this.lbl_models_count.Text = "Total: 0";
            this.lbl_models_count.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // lbl_template
            // 
            this.lbl_template.Font = new System.Drawing.Font("Segoe UI", 12.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_template.Location = new System.Drawing.Point(250, 50);
            this.lbl_template.Name = "lbl_template";
            this.lbl_template.Size = new System.Drawing.Size(280, 24);
            this.lbl_template.TabIndex = 5;
            this.lbl_template.Text = "Plantilla: ";
            // 
            // lbl_color
            // 
            this.lbl_color.Font = new System.Drawing.Font("Segoe UI", 12.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_color.Location = new System.Drawing.Point(250, 130);
            this.lbl_color.Name = "lbl_color";
            this.lbl_color.Size = new System.Drawing.Size(60, 24);
            this.lbl_color.TabIndex = 7;
            this.lbl_color.Text = "Color:";
            // 
            // lbl_origin
            // 
            this.lbl_origin.Font = new System.Drawing.Font("Segoe UI", 12.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_origin.Location = new System.Drawing.Point(250, 90);
            this.lbl_origin.Name = "lbl_origin";
            this.lbl_origin.Size = new System.Drawing.Size(280, 24);
            this.lbl_origin.TabIndex = 8;
            this.lbl_origin.Text = "Origen HQR:";
            // 
            // pctbx_color
            // 
            this.pctbx_color.Location = new System.Drawing.Point(310, 130);
            this.pctbx_color.Name = "pctbx_color";
            this.pctbx_color.Size = new System.Drawing.Size(220, 24);
            this.pctbx_color.TabIndex = 9;
            this.pctbx_color.TabStop = false;
            // 
            // pctbx_output
            // 
            this.pctbx_output.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pctbx_output.Location = new System.Drawing.Point(550, 50);
            this.pctbx_output.Name = "pctbx_output";
            this.pctbx_output.Size = new System.Drawing.Size(325, 325);
            this.pctbx_output.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pctbx_output.TabIndex = 10;
            this.pctbx_output.TabStop = false;
            // 
            // lbl_passwd_title
            // 
            this.lbl_passwd_title.Font = new System.Drawing.Font("Segoe UI", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_passwd_title.Location = new System.Drawing.Point(330, 190);
            this.lbl_passwd_title.Name = "lbl_passwd_title";
            this.lbl_passwd_title.Size = new System.Drawing.Size(120, 26);
            this.lbl_passwd_title.TabIndex = 11;
            this.lbl_passwd_title.Text = "Contraseña";
            this.lbl_passwd_title.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // txtbx_passwd
            // 
            this.txtbx_passwd.BackColor = System.Drawing.Color.Gainsboro;
            this.txtbx_passwd.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtbx_passwd.ForeColor = System.Drawing.Color.DimGray;
            this.txtbx_passwd.Location = new System.Drawing.Point(250, 230);
            this.txtbx_passwd.MaxLength = 30;
            this.txtbx_passwd.Name = "txtbx_passwd";
            this.txtbx_passwd.Size = new System.Drawing.Size(260, 22);
            this.txtbx_passwd.TabIndex = 12;
            this.txtbx_passwd.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.txtbx_passwd.TextChanged += new System.EventHandler(this.txtbx_passwd_TextChanged);
            // 
            // chkbx_passwd
            // 
            this.chkbx_passwd.AutoSize = true;
            this.chkbx_passwd.Cursor = System.Windows.Forms.Cursors.Hand;
            this.chkbx_passwd.Location = new System.Drawing.Point(513, 234);
            this.chkbx_passwd.Name = "chkbx_passwd";
            this.chkbx_passwd.Size = new System.Drawing.Size(15, 14);
            this.chkbx_passwd.TabIndex = 13;
            this.chkbx_passwd.UseVisualStyleBackColor = true;
            this.chkbx_passwd.CheckedChanged += new System.EventHandler(this.chkbx_passwd_CheckedChanged);
            // 
            // lbl_details_title
            // 
            this.lbl_details_title.Font = new System.Drawing.Font("Segoe UI", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_details_title.Location = new System.Drawing.Point(330, 16);
            this.lbl_details_title.Name = "lbl_details_title";
            this.lbl_details_title.Size = new System.Drawing.Size(120, 26);
            this.lbl_details_title.TabIndex = 14;
            this.lbl_details_title.Text = "Detalles";
            this.lbl_details_title.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // btn_generate
            // 
            this.btn_generate.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btn_generate.FlatAppearance.BorderSize = 2;
            this.btn_generate.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_generate.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(227)))), ((int)(((byte)(181)))), ((int)(((byte)(106)))));
            this.btn_generate.Location = new System.Drawing.Point(325, 410);
            this.btn_generate.Name = "btn_generate";
            this.btn_generate.Size = new System.Drawing.Size(130, 36);
            this.btn_generate.TabIndex = 16;
            this.btn_generate.Text = "Generar";
            this.btn_generate.UseVisualStyleBackColor = true;
            this.btn_generate.Click += new System.EventHandler(this.btn_generate_Click);
            // 
            // lbl_output_title
            // 
            this.lbl_output_title.Font = new System.Drawing.Font("Segoe UI", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_output_title.Location = new System.Drawing.Point(652, 16);
            this.lbl_output_title.Name = "lbl_output_title";
            this.lbl_output_title.Size = new System.Drawing.Size(120, 26);
            this.lbl_output_title.TabIndex = 17;
            this.lbl_output_title.Text = "Salida";
            this.lbl_output_title.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // pctbx_status
            // 
            this.pctbx_status.BackColor = System.Drawing.Color.DimGray;
            this.pctbx_status.Location = new System.Drawing.Point(0, 496);
            this.pctbx_status.Name = "pctbx_status";
            this.pctbx_status.Size = new System.Drawing.Size(884, 24);
            this.pctbx_status.TabIndex = 19;
            this.pctbx_status.TabStop = false;
            // 
            // lbl_status
            // 
            this.lbl_status.BackColor = System.Drawing.Color.DimGray;
            this.lbl_status.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_status.ForeColor = System.Drawing.Color.White;
            this.lbl_status.Location = new System.Drawing.Point(440, 496);
            this.lbl_status.Name = "lbl_status";
            this.lbl_status.Size = new System.Drawing.Size(442, 24);
            this.lbl_status.TabIndex = 20;
            this.lbl_status.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // txtbx_id
            // 
            this.txtbx_id.BackColor = System.Drawing.Color.Gainsboro;
            this.txtbx_id.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtbx_id.ForeColor = System.Drawing.Color.DimGray;
            this.txtbx_id.Location = new System.Drawing.Point(250, 330);
            this.txtbx_id.MaxLength = 30;
            this.txtbx_id.Name = "txtbx_id";
            this.txtbx_id.Size = new System.Drawing.Size(280, 22);
            this.txtbx_id.TabIndex = 22;
            this.txtbx_id.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.txtbx_id.TextChanged += new System.EventHandler(this.txtbx_id_TextChanged);
            // 
            // lbl_id_title
            // 
            this.lbl_id_title.Font = new System.Drawing.Font("Segoe UI", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_id_title.Location = new System.Drawing.Point(330, 290);
            this.lbl_id_title.Name = "lbl_id_title";
            this.lbl_id_title.Size = new System.Drawing.Size(120, 26);
            this.lbl_id_title.TabIndex = 21;
            this.lbl_id_title.Text = "ID";
            this.lbl_id_title.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // lbl_output_time
            // 
            this.lbl_output_time.Font = new System.Drawing.Font("Segoe UI", 12.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_output_time.Location = new System.Drawing.Point(550, 390);
            this.lbl_output_time.Name = "lbl_output_time";
            this.lbl_output_time.Size = new System.Drawing.Size(325, 24);
            this.lbl_output_time.TabIndex = 23;
            this.lbl_output_time.Text = "Tiempo:";
            // 
            // lbl_output_result
            // 
            this.lbl_output_result.Font = new System.Drawing.Font("Segoe UI", 12.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_output_result.Location = new System.Drawing.Point(550, 430);
            this.lbl_output_result.Name = "lbl_output_result";
            this.lbl_output_result.Size = new System.Drawing.Size(325, 24);
            this.lbl_output_result.TabIndex = 24;
            this.lbl_output_result.Text = "Integridad correcta:";
            // 
            // Generator
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 21F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(884, 520);
            this.Controls.Add(this.lbl_output_result);
            this.Controls.Add(this.lbl_output_time);
            this.Controls.Add(this.txtbx_id);
            this.Controls.Add(this.lbl_id_title);
            this.Controls.Add(this.lbl_status);
            this.Controls.Add(this.pctbx_status);
            this.Controls.Add(this.lbl_output_title);
            this.Controls.Add(this.btn_generate);
            this.Controls.Add(this.lbl_details_title);
            this.Controls.Add(this.chkbx_passwd);
            this.Controls.Add(this.txtbx_passwd);
            this.Controls.Add(this.lbl_passwd_title);
            this.Controls.Add(this.pctbx_output);
            this.Controls.Add(this.pctbx_color);
            this.Controls.Add(this.lbl_origin);
            this.Controls.Add(this.lbl_color);
            this.Controls.Add(this.lbl_template);
            this.Controls.Add(this.lbl_models_count);
            this.Controls.Add(this.lbl_models_refresh);
            this.Controls.Add(this.lbl_models_title);
            this.Controls.Add(this.lstbx_models_list);
            this.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ForeColor = System.Drawing.Color.DimGray;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.Name = "Generator";
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            ((System.ComponentModel.ISupportInitialize)(this.pctbx_color)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pctbx_output)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pctbx_status)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ListBox lstbx_models_list;
        private System.Windows.Forms.Label lbl_models_title;
        private System.Windows.Forms.Label lbl_models_refresh;
        private System.Windows.Forms.Label lbl_models_count;
        private System.Windows.Forms.Label lbl_template;
        private System.Windows.Forms.Label lbl_color;
        private System.Windows.Forms.Label lbl_origin;
        private System.Windows.Forms.PictureBox pctbx_color;
        private System.Windows.Forms.PictureBox pctbx_output;
        private System.Windows.Forms.Label lbl_passwd_title;
        private System.Windows.Forms.TextBox txtbx_passwd;
        private System.Windows.Forms.CheckBox chkbx_passwd;
        private System.Windows.Forms.Label lbl_details_title;
        private System.Windows.Forms.Button btn_generate;
        private System.Windows.Forms.Label lbl_output_title;
        private System.Windows.Forms.PictureBox pctbx_status;
        private System.Windows.Forms.Label lbl_status;
        private System.Windows.Forms.TextBox txtbx_id;
        private System.Windows.Forms.Label lbl_id_title;
        private System.Windows.Forms.Label lbl_output_time;
        private System.Windows.Forms.Label lbl_output_result;
    }
}