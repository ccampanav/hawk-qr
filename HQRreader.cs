﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AForge;
using AForge.Imaging;
using AForge.Imaging.Filters;
using AForge.Math.Geometry;
using Point = System.Drawing.Point;

namespace HawkQR
{
    public class HQRreader
    {
        private string passwd;
        private string chars;
        private Bitmap frame_original;
        private bool id_gotten;

        //Establecer contraseña. Calcular llave de descifrado
        public HQRreader(string user_passwd)
        {
            chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789.";
            passwd = user_passwd;
            rings_bits = new int[] { 59, 52, 45, 38, 31 };
            rings_dist = new int[5];
            CalcKeyEncode();
            CalcKeyDecode();
        }

        //Procesar imagen en buscar de código HQR (si es así obtener el id plasmado)
        public bool Process(Bitmap frame)
        {
            id_gotten = false;
            frame_original = frame;
            FilterImage();
            if (GetPattern())
            {
                ExtractData();
                id_gotten = DecodeID();
            }
            return id_gotten;
        }

        //######################### 1.1. LLAVE DE CIFRADO #########################

        //Calcular llave de codificación
        private List<int> key_encode;
        private void CalcKeyEncode()
        {
            //############### Cifrado estático
            //Crear lista del 0-224 que representa el índice del ID binarizado
            key_encode = new List<int>();
            for (int i = 0; i < 225; i++)
            {
                key_encode.Add(i);
            }
            //Invertir lista
            List<int> lst_tmp = new List<int>();
            for (int i = key_encode.Count - 1; i >= 0; i--)
            {
                lst_tmp.Add(key_encode[i]);
            }
            //Separar por índices impares y pares
            List<int> lst_odd = new List<int>();
            List<int> lst_even = new List<int>();
            for (int i = 0; i < lst_tmp.Count; i++)
            {
                if ((i + 1) % 2 == 0)
                {
                    lst_even.Add(lst_tmp[i]);
                }
                else
                {
                    lst_odd.Add(lst_tmp[i]);
                }
            }
            //Volver a concatenar de listas impares y pares
            key_encode.Clear();
            for (int i = 0; i < lst_odd.Count; i++)
            {
                key_encode.Add(lst_odd[i]);
            }
            for (int i = 0; i < lst_even.Count; i++)
            {
                key_encode.Add(lst_even[i]);
            }
            //############### Cifrado dinámico
            //Calcular SUM y MPWD de la contraseña
            int x, sum = 0;
            List<int> pwd = new List<int>(), mpwd = new List<int>();
            for (int i = 0; i < passwd.Length; i++)
            {
                x = int.Parse(passwd.Substring(i, 1));
                pwd.Add(x); sum += x;
            }
            mpwd = new List<int>();
            x = pwd.Count % 3;
            if (x == 1)
            {
                pwd.Add(0); pwd.Add(0);
            }
            else if (x == 2)
            {
                pwd.Add(0);
            }
            x = 0;
            for (int i = 0; i < pwd.Count / 3; i++)
            {
                mpwd.Add(pwd[x] + pwd[x + 1] + pwd[x + 2]);
                x += 3;
            }
            if (x == 1)
            {
                pwd.RemoveAt(pwd.Count - 1); pwd.RemoveAt(pwd.Count - 1);
            }
            else if (x == 2)
            {
                pwd.RemoveAt(pwd.Count - 1);
            }
            //Realizar iteraciones de cifrado dinámico        
            int m, n, ite;
            for (int j = 0; j < mpwd.Count; j++)
            {
                m = mpwd[j]; ite = j + 1;
                //Rotación a la izquierda
                n = sum - m + ite;
                LeftRotation(n);
                //Invertir en grupos
                if (m == 0)
                {
                    n = sum + ite;
                }
                else
                {
                    n = ((sum - (sum % m)) / m) + ite;
                }
                ReverseInGroups(n);
                //Rotación a la derecha
                n = sum - ite + m;
                RightRotation(n);
                //Intercambio
                ExchangeBits(ite, pwd, mpwd, sum);
                //Invertir todo
                FullReverse();
            }
        }

        //Rotar a la izquierda N posiciones
        private void LeftRotation(int n)
        {
            int aux;
            for (int x = 0; x < n; x++)
            {
                aux = key_encode[0];
                key_encode.RemoveAt(0);
                key_encode.Add(aux);
            }
        }

        //Rotar a la derecha N posiciones
        private void RightRotation(int n)
        {
            int aux;
            for (int x = 0; x < n; x++)
            {
                aux = key_encode[key_encode.Count - 1];
                key_encode.RemoveAt(key_encode.Count - 1);
                key_encode.Insert(0, aux);
            }
        }

        //Invertir en grupos de N caracteres
        private void ReverseInGroups(int n)
        {
            int rgi = (key_encode.Count - (key_encode.Count % n)) / n;
            List<int> lst_tmp;
            int limMin = 0, limMax = n - 1, aux;
            for (int k = 0; k < rgi; k++)
            {
                //Guardar caracteres de grupo en temporal
                lst_tmp = new List<int>();
                for (int l = limMax; l >= limMin; l--)
                {
                    lst_tmp.Add(key_encode[l]);
                }
                aux = -1;
                //Escribir caracteres de grupo temporal de manera invertida
                for (int l = limMin; l <= limMax; l++)
                {
                    aux++; key_encode[l] = lst_tmp[aux];
                }
                limMin += n; limMax += n;
            }
        }

        //Intercambio de caracteres (bits)
        private void ExchangeBits(int i, List<int> pwd, List<int> mpwd, int sum)
        {
            List<int> ecp = new List<int>(), ecj = new List<int>();
            //Calcular lista de índices de origen y salto
            for (int p = 0; p < pwd.Count; p++)
            {
                ecp.Add((pwd[p] * i) + sum);
                ecj.Insert(0, (pwd[p] + i) + mpwd[i - 1]);
            }
            int t, ia, ib;
            //Realizar intercambios
            for (int p = 0; p < ecp.Count; p++)
            {
                ia = ecp[p]; ib = ia + ecj[p];
                while (ia >= key_encode.Count)
                {
                    ia -= key_encode.Count;
                }
                while (ib >= key_encode.Count)
                {
                    ib -= key_encode.Count;
                }
                t = key_encode[ia];
                key_encode[ia] = key_encode[ib];
                key_encode[ib] = t;
            }
        }

        //Invertir lista completa
        private void FullReverse()
        {
            List<int> lst_tmp = new List<int>();
            for (int i = key_encode.Count - 1; i >= 0; i--)
            {
                lst_tmp.Add(key_encode[i]);
            }
            key_encode.Clear();
            for (int i = 0; i < lst_tmp.Count; i++)
            {
                key_encode.Add(lst_tmp[i]);
            }
        }

        //######################### 1.2. LLAVE DE DESCIFRADO #########################

        //Obtener llave de decodificación
        private List<int> key_decode;
        private void CalcKeyDecode()
        {
            key_decode = new List<int>();
            //Iterar del 0-224
            for (int i = 0; i < key_encode.Count; i++)
            {
                //Obtener el índice del número deseado
                for (int d = 0; d < key_encode.Count; d++)
                {
                    if (key_encode[d] == i)
                    {
                        key_decode.Add(d); break;
                    }
                }
            }
        }

        //######################### 2. PREPARACIÓN #########################

        //Preparar filtros y ajustar resolución
        private Crop filter_crop;
        private Grayscale filter_grayscale;
        private LevelsLinear filter_intensify;
        private BradleyLocalThresholding filter_binary;
        public void Prepare(int width, int height)
        {
            //Inicializar filtros
            filter_crop = new Crop(new Rectangle((width - height) / 2, 0, height, height));
            filter_grayscale = new Grayscale(0.2125, 0.7154, 0.0721);
            filter_intensify = new LevelsLinear();
            filter_intensify.InRed = new IntRange(127, 255);
            filter_intensify.InGreen = new IntRange(127, 255);
            filter_intensify.InBlue = new IntRange(127, 255);
            filter_binary = new BradleyLocalThresholding();
            filter_edges = new Edges();
            checker_circle = new SimpleShapeChecker();
            filter_blobs = new BlobCounter();
            filter_blobs.FilterBlobs = true;
            //Tamaño máximo del 100% usando el 100% de la webcam
            filter_blobs.MaxWidth = (int)(height * 1.0);
            filter_blobs.MaxHeight = (int)(height * 1.0);
            //Tamaño mínimo del 10% usando el 40% de la webcam
            filter_blobs.MinWidth = (int)(height * 0.10 * 0.4);
            filter_blobs.MinHeight = (int)(height * 0.10 * 0.4);       
        }

        //######################### 3. FILTRAR #########################

        //Binarizar imagen original
        public Bitmap frame_original_cropped;
        private Bitmap frame_original_binarized;
        private void FilterImage()
        {
            //Recortar cuadrado central, aplicar escala de grises, intensificar colores oscuros y binarizar eliminando sombras
            frame_original_cropped = filter_crop.Apply(frame_original);
            frame_original_binarized = (Bitmap)frame_original_cropped.Clone();
            frame_original_binarized = filter_grayscale.Apply(frame_original_binarized);
            frame_original_binarized = filter_intensify.Apply(frame_original_binarized);
            frame_original_binarized = filter_binary.Apply(frame_original_binarized);
        }

        //######################### 4. BUSQUEDA #########################

        //Obtener posible patrón en imagen original binarizada
        private Bitmap frame_hqr;
        private Edges filter_edges;
        private SimpleShapeChecker checker_circle;
        private BlobCounter filter_blobs;
        private Blob[] blobs_original, blobs_hqr;
        private List<circle> circles;
        private RotateBilinear filter_rotate;
        private Crop filter_crop_2;
        private ResizeBilinear filter_resize;
        private bool GetPattern()
        {
            bool answ = false;
            //Segmentar imagen original binarizada
            filter_blobs.ProcessImage(filter_edges.Apply(frame_original_binarized));
            blobs_original = filter_blobs.GetObjectsInformation();
            if (blobs_original.Length >= 3)
            {
                circles = new List<circle>();
                float min_wh, max_wh;
                //Filtrar círculos de lista de objetos
                AForge.Point circle_center;
                float circle_radius;
                for (int i = 0; i < blobs_original.Length; i++)
                {
                    List<IntPoint> edge_points = filter_blobs.GetBlobsEdgePoints(blobs_original[i]);
                    min_wh = (float)(blobs_original[i].Rectangle.Width * 0.95f); //Tolerancias para ancho y alto (5%)
                    max_wh = (float)(blobs_original[i].Rectangle.Width * 1.05f);
                    if (blobs_original[i].Rectangle.Height >= min_wh && blobs_original[i].Rectangle.Height <= max_wh)
                    {
                        if (checker_circle.IsCircle(edge_points, out circle_center, out circle_radius) == true)
                        {
                            circles.Add(new circle((int)circle_center.X, (int)circle_center.Y, circle_radius, blobs_original[i]));
                        }
                    }
                }
                //Ordenar lista de círculos de mayor a menor radio
                circles = circles.OrderBy(o => o.radius).ToList();
                circles.Reverse();
                //Se requieren al menos 3 círculos para buscar posible patrón
                if (circles.Count >= 3)
                {
                    circle circle_hqr = null, circle_ar_a = null, circle_ar_b = null;
                    for (int h = 0; h < circles.Count - 2; h++) //Determinar posible HQR
                    {
                        min_wh = (float)((circles[h].radius * 0.151485) * 0.95f); //Tolerancias relación para ARa (5%)
                        max_wh = (float)((circles[h].radius * 0.151485) * 1.05f);
                        for (int a = h + 1; a < circles.Count - 1; a++)
                        {
                            if (circles[a].radius >= min_wh && circles[a].radius <= max_wh) //ARa encontrado
                            {
                                min_wh = (float)((circles[h].radius * 0.100990) * 0.95f); //Tolerancias relación para ARb (5%)
                                max_wh = (float)((circles[h].radius * 0.100990) * 1.05f);
                                for (int b = a + 1; b < circles.Count; b++)
                                {
                                    if (circles[b].radius >= min_wh && circles[b].radius <= max_wh) //ARb encontrado
                                    {
                                        circle_hqr = circles[h];
                                        circle_ar_a = circles[a];
                                        circle_ar_b = circles[b];
                                        break;
                                    }
                                }
                            }
                        }
                    }
                    //Patrón encontrado por relaciones de ambos AR hacia HQR
                    if (circle_hqr != null && circle_ar_a != null && circle_ar_b != null)
                    {
                        float distRel = (float)((circle_hqr.radius * 2) * 0.606991); //Relación del diametro de HQR del 60.6991%
                        float distHA = CalculateDistance(circle_hqr.location, circle_ar_a.location);
                        float distHB = CalculateDistance(circle_hqr.location, circle_ar_b.location);
                        min_wh = (float)(distRel * 0.95f); //Tolerancias distancia HQR a ARb (5%)
                        max_wh = (float)(distRel * 1.05f);
                        if ((distHA >= min_wh && distHA <= max_wh) && (distHB >= min_wh && distHB <= max_wh))
                        {
                            //Transformar coordenadas de ambos AR de imagen a plano
                            circle_ar_a.location = ConvertCoordenates(circle_hqr.location, circle_ar_a.location, true);
                            circle_ar_b.location = ConvertCoordenates(circle_hqr.location, circle_ar_b.location, true);
                            float angle = CalculateAngle(circle_ar_a.location, circle_ar_b.location);
                            //Rotar imagen a posición original
                            filter_rotate = new RotateBilinear(-angle, true);
                            frame_hqr = filter_rotate.Apply(frame_original_binarized);
                            //Encontrar objeto HQR en imagen rotada
                            filter_blobs.ProcessImage(filter_edges.Apply(frame_hqr));
                            blobs_hqr = filter_blobs.GetObjectsInformation();
                            circle_hqr.location = new Point(-1, -1);
                            if (blobs_hqr.Length > 0)
                            {
                                //Filtrar objetos en busca de HQR rotado
                                min_wh = (float)(circle_hqr.blob.Rectangle.Width * 0.95f); //Tolerancias ancho HQR blob (5%)
                                max_wh = (float)(circle_hqr.blob.Rectangle.Width * 1.05f);
                                for (int i = 0; i < blobs_hqr.Length; i++)
                                {
                                    if (blobs_hqr[i].Rectangle.Width >= min_wh && blobs_hqr[i].Rectangle.Width <= max_wh)
                                    {
                                        //Filtrar objetos en busca de HQR rotado
                                        min_wh = (float)(circle_hqr.blob.Rectangle.Height * 0.95f); //Tolerancias alto HQR blob (5%)
                                        max_wh = (float)(circle_hqr.blob.Rectangle.Height * 1.05f);
                                        if (blobs_hqr[i].Rectangle.Height >= min_wh && blobs_hqr[i].Rectangle.Height <= max_wh)
                                        {
                                            circle_hqr.location.X = 0; circle_hqr.location.Y = 0;
                                            circle_hqr.blob = blobs_hqr[i]; break;
                                        }
                                    }
                                }
                                //Origen de HQR rotado encontrado
                                if (circle_hqr.location.X == 0 && circle_hqr.location.Y == 0)
                                {
                                    //Recortar HQR
                                    filter_crop_2 = new Crop(new Rectangle(
                                        (int)circle_hqr.blob.Rectangle.X,
                                        (int)circle_hqr.blob.Rectangle.Y,
                                        (int)circle_hqr.blob.Rectangle.Width,
                                        (int)circle_hqr.blob.Rectangle.Height
                                        ));
                                    frame_hqr = filter_crop_2.Apply(frame_hqr);
                                    int hqrW = (int)circle_hqr.blob.Rectangle.Width;
                                    int hqrH = (int)circle_hqr.blob.Rectangle.Height;
                                    //Redimensionar a una imagen totalmente cuadrada
                                    if (hqrW > hqrH)
                                    {
                                        filter_resize = new ResizeBilinear(hqrW, hqrW);
                                        frame_hqr = filter_resize.Apply(frame_hqr);
                                    }
                                    else if (hqrW < hqrH)
                                    {
                                        filter_resize = new ResizeBilinear(hqrH, hqrH);
                                        frame_hqr = filter_resize.Apply(frame_hqr);
                                    }
                                    answ = true;
                                }
                            }
                        }
                    }
                }
            }
            return answ;
        }

        //Calcular distancia entre dos puntos
        private float CalculateDistance(Point o, Point p) 
        {
            return (float)Math.Sqrt(Math.Pow(o.X - p.X, 2) + Math.Pow(o.Y - p.Y, 2));
        }

        //Transformar coordenadas entre plano cartesiano e imagen
        private Point ConvertCoordenates(Point init, Point loc, bool mode)
        {
            Point answ = new Point();
            if (mode == true) //De imagen a plano cartesiano
            {
                answ.X = loc.X - init.X;
                answ.Y = init.Y - loc.Y;
            }
            else //De plano cartesiano a imagen
            {
                answ.X = init.X + loc.X;
                answ.Y = init.Y - loc.Y;
            }
            return answ;
        }

        //Calcular angulo entre 2 puntos
        private float CalculateAngle(Point o, Point p) 
        {
            float ang = 0;
            float a = Math.Abs(p.X - o.X), b = Math.Abs(p.Y - o.Y);
            if (a == 0f)
            {
                if (p.Y > o.Y)
                {
                    ang = 90f;
                }
                else
                {
                    ang = 270f;
                }
            }
            else
            {
                ang = (float)Math.Atan(b / a) * 180f / (float)Math.PI;
                if (p.X < o.X && p.Y >= o.Y) //Cuadrante 2
                {
                    ang = 180 - ang;
                }
                if (p.X < o.X && p.Y < o.Y) //Cuadrante 3
                {
                    ang += 180;
                }
                if (p.X >= o.X && p.Y < o.Y) //Cuadrante 4
                {
                    ang = 360 - ang;
                }
            }
            return ang;
        }

        //######################### 5. EXTRACCIÓN #########################

        //Extraer bits del posible patrón encontrado
        private string id_encoded;
        private int[] rings_bits;
        private int[] rings_dist;
        private void ExtractData()
        {
            id_encoded = "";
            //Configurar información de anillos            
            rings_dist[0] = (int)Math.Round(frame_hqr.Width * 0.465346);
            rings_dist[1] = (int)Math.Round(frame_hqr.Width * 0.411089);
            rings_dist[2] = (int)Math.Round(frame_hqr.Width * 0.356435);
            rings_dist[3] = (int)Math.Round(frame_hqr.Width * 0.301980);
            rings_dist[4] = (int)Math.Round(frame_hqr.Width * 0.247524);
            //Obtener punto de origen (central de hqr) y convertir mapa de bits a matriz
            int[,] matrix = Bitmap2Matrix(frame_hqr);
            int radius = (int)Math.Round(frame_hqr.Width * 0.5);
            Point point_init = new Point(radius - 1, radius - 1);
            Point point_ring_radius, point_aux;
            //Extrer información del HQR de los 5 anillos
            for (int ring = 0; ring < 5; ring++)
            {
                point_ring_radius = new Point(rings_dist[ring], 0);
                float angleStep = 360f / rings_bits[ring], angleCurr = 0;
                for (int b = 0; b < rings_bits[ring]; b++)
                {
                    point_aux = RotatePoint(point_ring_radius, angleCurr);
                    point_aux = ConvertCoordenates(point_init, point_aux, false);
                    id_encoded += matrix[point_aux.X, point_aux.Y].ToString();
                    angleCurr += angleStep;
                }
            }
        }

        //Método inseguro para convertir mapa de bits a matriz de bytes binarizada
        private unsafe int[,] Bitmap2Matrix(Bitmap bitmap)
        {
            int[,] answ = new int[bitmap.Width, bitmap.Height];
            //Obtener información del bitmap
            Rectangle rectangle = new Rectangle(0, 0, bitmap.Width, bitmap.Height);
            BitmapData hqr_data = bitmap.LockBits(
                rectangle,
                ImageLockMode.ReadOnly,
                PixelFormat.Format32bppArgb
                );
            try
            {
                var scan0_hqr = (int*)hqr_data.Scan0;
                var stride = hqr_data.Stride / 4;
                int black = Color.Black.ToArgb();
                for (int y = 0; y < rectangle.Bottom; y++)
                {
                    for (int x = 0; x < rectangle.Right; x++)
                    {
                        if (*(scan0_hqr + x + y * stride) == black)
                        {
                            answ[x, y] = 1;
                        }
                        else
                        {
                            answ[x, y] = 0;
                        }
                    }
                }
                return answ;
            }
            finally
            {
                bitmap.UnlockBits(hqr_data);
            }
        }

        //Rotar un punto dado un angulo
        private Point RotatePoint(Point p, float ang)
        {
            ang = ang * (float)Math.PI / 180f;
            return new Point((int)Math.Round(p.X * Math.Cos(ang) - p.Y * Math.Sin(ang)),
                (int)Math.Round(p.X * Math.Sin(ang) + p.Y * Math.Cos(ang)));
        }

        //######################### 6. DECODIFICACIÓN #########################

        //Aplicar llave de decodificación a id codificado
        public string id_decoded;
        private bool DecodeID()
        {
            bool answ = false;
            //Decodificar id
            id_decoded = "";
            for (int i = 0; i < key_decode.Count; i++)
            {
                id_decoded += id_encoded.Substring(key_decode[i], 1);
            }
            //Separar id en 30 segmentos y 2 validadores
            List<string> segments = new List<string>();
            int aux = 0;
            for (int i = 0; i < 29; i++)
            {
                segments.Add(id_decoded.Substring(aux, 6)); //Primeros 29 segmentos (omitiendo bit aleatorio)
                aux += 7;
            }
            segments.Add(id_decoded.Substring(aux, 6)); //Segmento 30
            aux += 6;
            for (int i = 0; i < 2; i++)
            {
                segments.Add(id_decoded.Substring(aux, 8)); //2 validadores
                aux += 8;
            }
            //Realizar comprobación de integridad
            int sum_1 = Bin2Dec(segments[segments.Count - 1]); segments.RemoveAt(segments.Count - 1);
            int sum_0 = Bin2Dec(segments[segments.Count - 1]); segments.RemoveAt(segments.Count - 1);
            if (sum_0 + sum_1 == 180)
            {
                int s0 = 0, s1 = 0;
                for (int i = 0; i < segments.Count; i++)
                {
                    for (int c = 0; c < segments[i].Length; c++)
                    {
                        if (segments[i].Substring(c, 1).Equals("0"))
                        {
                            s0++;
                        }
                        if (segments[i].Substring(c, 1).Equals("1"))
                        {
                            s1++;
                        }
                    }
                }
                if (s0 == sum_0 && s1 == sum_1) //Comprobación exitosa
                {
                    id_decoded = "";
                    //Realizar decodificación de los 30 segmentos
                    for (int i = 0; i < segments.Count; i++)
                    {
                        aux = Bin2Dec(segments[i]) - 1;
                        if (aux >= 0)
                        {
                            id_decoded += chars.Substring(aux, 1);
                        }
                        else
                        {
                            //No hay caracter existente en la posición
                        }
                    }
                    answ = true;
                }
            }
            return answ;
        }

        //Convertir número binario a decimal
        private int Bin2Dec(string bin)
        {
            int dec = 0, k = 0;
            for (int i = bin.Length - 1; i >= 0; i--)
            {
                if (bin.Substring(i, 1).Equals("1") == true)
                {
                    dec += (int)Math.Pow(2, k);
                }
                k++;
            }
            return dec;
        }
    }

    //Clase para almacenar un círculo
    class circle
    {
        public Point location;
        public float radius;
        public Blob blob;

        public circle(int x, int y, float rad, Blob blb)
        {
            location = new Point(x, y);
            radius = rad;
            blob = blb;
        }
    }
}
