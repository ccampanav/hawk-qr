﻿namespace HawkQR
{
    partial class Reader
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Reader));
            this.lbl_status = new System.Windows.Forms.Label();
            this.pctbx_status = new System.Windows.Forms.PictureBox();
            this.btn_process = new System.Windows.Forms.Button();
            this.chkbx_passwd = new System.Windows.Forms.CheckBox();
            this.txtbx_passwd = new System.Windows.Forms.TextBox();
            this.lbl_passwd_title = new System.Windows.Forms.Label();
            this.lbl_webcam_title = new System.Windows.Forms.Label();
            this.pctbx_webcam = new System.Windows.Forms.PictureBox();
            this.lbl_source_title = new System.Windows.Forms.Label();
            this.cmbbx_source = new System.Windows.Forms.ComboBox();
            this.lbl_source_refresh = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.pctbx_status)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pctbx_webcam)).BeginInit();
            this.SuspendLayout();
            // 
            // lbl_status
            // 
            this.lbl_status.BackColor = System.Drawing.Color.DimGray;
            this.lbl_status.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_status.ForeColor = System.Drawing.Color.White;
            this.lbl_status.Location = new System.Drawing.Point(440, 496);
            this.lbl_status.Name = "lbl_status";
            this.lbl_status.Size = new System.Drawing.Size(442, 24);
            this.lbl_status.TabIndex = 22;
            this.lbl_status.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // pctbx_status
            // 
            this.pctbx_status.BackColor = System.Drawing.Color.DimGray;
            this.pctbx_status.Location = new System.Drawing.Point(0, 496);
            this.pctbx_status.Name = "pctbx_status";
            this.pctbx_status.Size = new System.Drawing.Size(884, 24);
            this.pctbx_status.TabIndex = 21;
            this.pctbx_status.TabStop = false;
            // 
            // btn_process
            // 
            this.btn_process.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btn_process.FlatAppearance.BorderSize = 2;
            this.btn_process.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_process.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(227)))), ((int)(((byte)(181)))), ((int)(((byte)(106)))));
            this.btn_process.Location = new System.Drawing.Point(145, 290);
            this.btn_process.Name = "btn_process";
            this.btn_process.Size = new System.Drawing.Size(130, 36);
            this.btn_process.TabIndex = 26;
            this.btn_process.Text = "Iniciar";
            this.btn_process.UseVisualStyleBackColor = true;
            this.btn_process.Click += new System.EventHandler(this.btn_process_Click);
            // 
            // chkbx_passwd
            // 
            this.chkbx_passwd.AutoSize = true;
            this.chkbx_passwd.Cursor = System.Windows.Forms.Cursors.Hand;
            this.chkbx_passwd.Location = new System.Drawing.Point(333, 214);
            this.chkbx_passwd.Name = "chkbx_passwd";
            this.chkbx_passwd.Size = new System.Drawing.Size(15, 14);
            this.chkbx_passwd.TabIndex = 25;
            this.chkbx_passwd.UseVisualStyleBackColor = true;
            this.chkbx_passwd.CheckedChanged += new System.EventHandler(this.chkbx_passwd_CheckedChanged);
            // 
            // txtbx_passwd
            // 
            this.txtbx_passwd.BackColor = System.Drawing.Color.Gainsboro;
            this.txtbx_passwd.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtbx_passwd.ForeColor = System.Drawing.Color.DimGray;
            this.txtbx_passwd.Location = new System.Drawing.Point(70, 210);
            this.txtbx_passwd.MaxLength = 30;
            this.txtbx_passwd.Name = "txtbx_passwd";
            this.txtbx_passwd.Size = new System.Drawing.Size(260, 22);
            this.txtbx_passwd.TabIndex = 24;
            this.txtbx_passwd.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.txtbx_passwd.TextChanged += new System.EventHandler(this.txtbx_passwd_TextChanged);
            // 
            // lbl_passwd_title
            // 
            this.lbl_passwd_title.Font = new System.Drawing.Font("Segoe UI", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_passwd_title.Location = new System.Drawing.Point(150, 170);
            this.lbl_passwd_title.Name = "lbl_passwd_title";
            this.lbl_passwd_title.Size = new System.Drawing.Size(120, 26);
            this.lbl_passwd_title.TabIndex = 23;
            this.lbl_passwd_title.Text = "Contraseña";
            this.lbl_passwd_title.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // lbl_webcam_title
            // 
            this.lbl_webcam_title.Font = new System.Drawing.Font("Segoe UI", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_webcam_title.Location = new System.Drawing.Point(540, 16);
            this.lbl_webcam_title.Name = "lbl_webcam_title";
            this.lbl_webcam_title.Size = new System.Drawing.Size(120, 26);
            this.lbl_webcam_title.TabIndex = 28;
            this.lbl_webcam_title.Text = "Webcam";
            this.lbl_webcam_title.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // pctbx_webcam
            // 
            this.pctbx_webcam.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pctbx_webcam.Location = new System.Drawing.Point(380, 50);
            this.pctbx_webcam.Name = "pctbx_webcam";
            this.pctbx_webcam.Size = new System.Drawing.Size(440, 440);
            this.pctbx_webcam.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pctbx_webcam.TabIndex = 27;
            this.pctbx_webcam.TabStop = false;
            // 
            // lbl_source_title
            // 
            this.lbl_source_title.Font = new System.Drawing.Font("Segoe UI", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_source_title.Location = new System.Drawing.Point(155, 16);
            this.lbl_source_title.Name = "lbl_source_title";
            this.lbl_source_title.Size = new System.Drawing.Size(120, 26);
            this.lbl_source_title.TabIndex = 31;
            this.lbl_source_title.Text = "Origen";
            this.lbl_source_title.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // cmbbx_source
            // 
            this.cmbbx_source.BackColor = System.Drawing.Color.Gainsboro;
            this.cmbbx_source.Cursor = System.Windows.Forms.Cursors.Hand;
            this.cmbbx_source.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbbx_source.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cmbbx_source.ForeColor = System.Drawing.Color.DimGray;
            this.cmbbx_source.FormattingEnabled = true;
            this.cmbbx_source.Location = new System.Drawing.Point(70, 50);
            this.cmbbx_source.Name = "cmbbx_source";
            this.cmbbx_source.Size = new System.Drawing.Size(278, 29);
            this.cmbbx_source.TabIndex = 32;
            // 
            // lbl_source_refresh
            // 
            this.lbl_source_refresh.Cursor = System.Windows.Forms.Cursors.Hand;
            this.lbl_source_refresh.Font = new System.Drawing.Font("Segoe UI", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_source_refresh.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(227)))), ((int)(((byte)(181)))), ((int)(((byte)(106)))));
            this.lbl_source_refresh.Location = new System.Drawing.Point(180, 85);
            this.lbl_source_refresh.Name = "lbl_source_refresh";
            this.lbl_source_refresh.Size = new System.Drawing.Size(70, 20);
            this.lbl_source_refresh.TabIndex = 33;
            this.lbl_source_refresh.Text = "Refrescar";
            this.lbl_source_refresh.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.lbl_source_refresh.Click += new System.EventHandler(this.lbl_source_refresh_Click);
            // 
            // Reader
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 21F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(884, 520);
            this.Controls.Add(this.lbl_source_refresh);
            this.Controls.Add(this.cmbbx_source);
            this.Controls.Add(this.lbl_source_title);
            this.Controls.Add(this.lbl_webcam_title);
            this.Controls.Add(this.pctbx_webcam);
            this.Controls.Add(this.btn_process);
            this.Controls.Add(this.chkbx_passwd);
            this.Controls.Add(this.txtbx_passwd);
            this.Controls.Add(this.lbl_passwd_title);
            this.Controls.Add(this.lbl_status);
            this.Controls.Add(this.pctbx_status);
            this.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ForeColor = System.Drawing.Color.DimGray;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.Name = "Reader";
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            ((System.ComponentModel.ISupportInitialize)(this.pctbx_status)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pctbx_webcam)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lbl_status;
        private System.Windows.Forms.PictureBox pctbx_status;
        private System.Windows.Forms.Button btn_process;
        private System.Windows.Forms.CheckBox chkbx_passwd;
        private System.Windows.Forms.TextBox txtbx_passwd;
        private System.Windows.Forms.Label lbl_passwd_title;
        private System.Windows.Forms.Label lbl_webcam_title;
        private System.Windows.Forms.PictureBox pctbx_webcam;
        private System.Windows.Forms.Label lbl_source_title;
        private System.Windows.Forms.ComboBox cmbbx_source;
        private System.Windows.Forms.Label lbl_source_refresh;
    }
}