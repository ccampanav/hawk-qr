﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Media;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace HawkQR
{
    public partial class Home : Form 
    {
        private Start frm_start;
        private Generator frm_generator;
        private Reader frm_reader;
        public SoundPlayer sound_button, sound_false, sound_true, sound_hqr;

        //Inicializar componenetes y mostrar formulario de inicio
        public Home() 
        {
            InitializeComponent();
            frm_start = new Start(this);
            frm_generator = new Generator(this);
            frm_reader = new Reader(this);
            LoadSounds();
            ShowStart();
        }

        //Cargar sonidos de archivos .wav
        private void LoadSounds() 
        {
            string path_sounds = "sounds/";
            sound_button = new SoundPlayer();
            sound_false = new SoundPlayer();
            sound_true = new SoundPlayer();
            sound_hqr = new SoundPlayer();
            if (File.Exists(path_sounds + "button.wav"))
            {
                sound_button.SoundLocation = path_sounds + "button.wav";
            }
            if (File.Exists(path_sounds + "false.wav"))
            {
                sound_false.SoundLocation = path_sounds + "false.wav";
            }
            if (File.Exists(path_sounds + "true.wav"))
            {
                sound_true.SoundLocation = path_sounds + "true.wav";
            }
            if (File.Exists(path_sounds + "hqr.wav"))
            {
                sound_hqr.SoundLocation = path_sounds + "hqr.wav";
            }
        }

        //Mostrar un formulario en el panel principal
        private void ShowForm(Form frm) 
        {
            pnl_home.Controls.Clear();
            frm.TopLevel = false;
            pnl_home.Controls.Add(frm);
            frm.Show();
        }

        //Mostrar formulario de inicio
        private void ShowStart() 
        {
            ShowForm(frm_start);
        }

        //Mostrar formulario generador
        private void lblGenerator_Click(object sender, EventArgs e) 
        {
            lbl_generator.ForeColor = Color.FromArgb(227, 181, 106);
            lbl_reader.ForeColor = Color.White;
            ShowForm(frm_generator);
        }

        //Mostrar formulario lector
        private void lblReader_Click(object sender, EventArgs e) 
        {
            lbl_generator.ForeColor = Color.White;
            lbl_reader.ForeColor = Color.FromArgb(227, 181, 106);
            ShowForm(frm_reader);
        }

        //Detener transmión de video en Reader
        private void Home_FormClosing(object sender, FormClosingEventArgs e) 
        {
            frm_reader.StopStream();
        }
    }
}
