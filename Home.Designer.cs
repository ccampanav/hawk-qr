﻿namespace HawkQR
{
    partial class Home
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Home));
            this.pnl_home = new System.Windows.Forms.Panel();
            this.lbl_generator = new System.Windows.Forms.Label();
            this.lbl_reader = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // pnl_home
            // 
            this.pnl_home.BackColor = System.Drawing.Color.White;
            this.pnl_home.Location = new System.Drawing.Point(0, 40);
            this.pnl_home.Margin = new System.Windows.Forms.Padding(2, 4, 2, 4);
            this.pnl_home.Name = "pnl_home";
            this.pnl_home.Size = new System.Drawing.Size(884, 520);
            this.pnl_home.TabIndex = 2;
            // 
            // lbl_generator
            // 
            this.lbl_generator.BackColor = System.Drawing.Color.DimGray;
            this.lbl_generator.Cursor = System.Windows.Forms.Cursors.Hand;
            this.lbl_generator.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.lbl_generator.Font = new System.Drawing.Font("Segoe UI Semibold", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_generator.ForeColor = System.Drawing.Color.White;
            this.lbl_generator.Location = new System.Drawing.Point(270, 2);
            this.lbl_generator.Name = "lbl_generator";
            this.lbl_generator.Size = new System.Drawing.Size(150, 32);
            this.lbl_generator.TabIndex = 3;
            this.lbl_generator.Text = "Generador";
            this.lbl_generator.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.lbl_generator.Click += new System.EventHandler(this.lblGenerator_Click);
            // 
            // lbl_reader
            // 
            this.lbl_reader.BackColor = System.Drawing.Color.DimGray;
            this.lbl_reader.Cursor = System.Windows.Forms.Cursors.Hand;
            this.lbl_reader.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.lbl_reader.Font = new System.Drawing.Font("Segoe UI Semibold", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_reader.ForeColor = System.Drawing.Color.White;
            this.lbl_reader.Location = new System.Drawing.Point(465, 2);
            this.lbl_reader.Name = "lbl_reader";
            this.lbl_reader.Size = new System.Drawing.Size(150, 32);
            this.lbl_reader.TabIndex = 4;
            this.lbl_reader.Text = "Lector";
            this.lbl_reader.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.lbl_reader.Click += new System.EventHandler(this.lblReader_Click);
            // 
            // Home
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(11F, 25F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.DimGray;
            this.ClientSize = new System.Drawing.Size(884, 561);
            this.Controls.Add(this.lbl_reader);
            this.Controls.Add(this.lbl_generator);
            this.Controls.Add(this.pnl_home);
            this.Font = new System.Drawing.Font("Segoe UI", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ForeColor = System.Drawing.Color.DimGray;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(6);
            this.MaximumSize = new System.Drawing.Size(900, 600);
            this.MinimumSize = new System.Drawing.Size(900, 600);
            this.Name = "Home";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "HawkQR";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Home_FormClosing);
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.Panel pnl_home;
        private System.Windows.Forms.Label lbl_generator;
        private System.Windows.Forms.Label lbl_reader;
    }
}

