﻿namespace HawkQR
{
    partial class Start
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Start));
            this.lbl_powered = new System.Windows.Forms.Label();
            this.pctbx_business = new System.Windows.Forms.PictureBox();
            this.pctbx_logo = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.pctbx_business)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pctbx_logo)).BeginInit();
            this.SuspendLayout();
            // 
            // lbl_powered
            // 
            this.lbl_powered.Cursor = System.Windows.Forms.Cursors.Arrow;
            this.lbl_powered.Font = new System.Drawing.Font("Segoe UI Semibold", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_powered.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(227)))), ((int)(((byte)(181)))), ((int)(((byte)(106)))));
            this.lbl_powered.Location = new System.Drawing.Point(372, 314);
            this.lbl_powered.Name = "lbl_powered";
            this.lbl_powered.Size = new System.Drawing.Size(140, 32);
            this.lbl_powered.TabIndex = 8;
            this.lbl_powered.Text = "Powered by:";
            this.lbl_powered.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // pctbx_business
            // 
            this.pctbx_business.Image = global::HawkQR.Properties.Resources.CCVsoftware;
            this.pctbx_business.Location = new System.Drawing.Point(362, 350);
            this.pctbx_business.Name = "pctbx_business";
            this.pctbx_business.Size = new System.Drawing.Size(160, 160);
            this.pctbx_business.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pctbx_business.TabIndex = 9;
            this.pctbx_business.TabStop = false;
            // 
            // pctbx_logo
            // 
            this.pctbx_logo.Image = global::HawkQR.Properties.Resources.icon;
            this.pctbx_logo.Location = new System.Drawing.Point(142, 60);
            this.pctbx_logo.Name = "pctbx_logo";
            this.pctbx_logo.Size = new System.Drawing.Size(600, 204);
            this.pctbx_logo.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pctbx_logo.TabIndex = 6;
            this.pctbx_logo.TabStop = false;
            // 
            // Start
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 21F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(884, 520);
            this.Controls.Add(this.pctbx_business);
            this.Controls.Add(this.lbl_powered);
            this.Controls.Add(this.pctbx_logo);
            this.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ForeColor = System.Drawing.Color.DimGray;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.Name = "Start";
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            ((System.ComponentModel.ISupportInitialize)(this.pctbx_business)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pctbx_logo)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.PictureBox pctbx_logo;
        private System.Windows.Forms.Label lbl_powered;
        private System.Windows.Forms.PictureBox pctbx_business;
    }
}