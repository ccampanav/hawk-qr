﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Drawing;

namespace HawkQR
{
    //Clase con métodos auxiliares de distintos tipos
    public class Auxiliar
    {
        private string filter_passwd, filter_id;

        public Auxiliar()
        {
            filter_passwd = "0123456789";
            filter_id = filter_passwd + "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz.";
        }

        //######################### ARCHIVOS #########################

        //Verificar si existe un archivo
        public bool FileExist(string name)
        {
            bool answ = false;
            if (File.Exists(name))
            {
                answ = true;
            }
            return answ;
        }

        //Verificar si existe un directorio
        public bool DirectoryExist(string name)
        {
            bool answ = false;
            if (Directory.Exists(name))
            {
                answ = true;
            }
            return answ;
        }

        //Cargar imagen en bitmap dada su ruta
        public Bitmap LoadImageFromFile(string name)
        {
            Bitmap answ = null;
            try
            {
                Image img = Image.FromFile(name);
                answ = new Bitmap(img);
                img.Dispose();
            }
            catch (Exception ex) { }
            return answ;
        }

        //Obtener lista de archivos de un directorio dada su extensión
        public List<string> GetFiles(string path, string extension)
        {
            List<string> answ = null;
            if (DirectoryExist(path))
            {
                string[] list = Directory.GetFiles(path);
                if (list.Length > 0)
                {
                    answ = new List<string>();
                    //Quitar ruta completa y dejar solo nombre de archivo
                    string[] aux;
                    for (int f = 0; f < list.Length; f++)
                    {
                        aux = list[f].Split('\\');
                        if (aux.Length > 0)
                        {
                            list[f] = aux[aux.Length - 1];
                            //Verificar extensión
                            aux = list[f].Split('.');
                            if (aux.Length == 2)
                            {
                                if (aux[1].Equals(extension))
                                {
                                    answ.Add(list[f]);
                                }
                            }
                        }
                    }
                }
            }
            return answ;
        }

        //Obtener contenido de un archivo de texto plano y un número definido de líneas
        public string[] GetContentOfFile(string name, int lines)
        {
            string[] answ = File.ReadAllLines(name);
            if (answ.Length != lines)
            {
                answ = null;
            }
            return answ;
        }

        //######################### CADENAS #########################

        //Obtener punto dada una cadena
        public Point String2Point(string text, int lim_width, int lim_height)
        {
            int x = -1, y = -1;
            Point answ = new Point(-1, -1);
            string[] tspl = text.Split(',');
            if (tspl.Length == 2)
            {
                try
                {
                    x = int.Parse(tspl[0]);
                    y = int.Parse(tspl[1]);
                }
                catch (Exception ex) { }
                if (x >= 0 && y >= 0 && x <= lim_width && y <= lim_height)
                {
                    answ.X = x;
                    answ.Y = y;
                }
            }
            return answ;
        }

        //Obtener color dada una cadena
        public Color String2Color(string text)
        {
            int r = -1, g = -1, b = -1;
            Color answ = Color.Empty;
            string[] tspl = text.Split(',');
            if (tspl.Length == 3)
            {
                try
                {
                    r = int.Parse(tspl[0]);
                    g = int.Parse(tspl[1]);
                    b = int.Parse(tspl[2]);
                }
                catch (Exception ex) { }
                if (r >= 0 && g >= 0 && b >= 0 && r <= 255 && g <= 255 && b <= 255)
                {
                    answ = Color.FromArgb(r, g, b);
                }
            }          
            return answ;
        }

        //Aplicar un filtro de caracteres permitidos a una cadena 
        public string FilterString(string text, char filter)
        {
            string answ = "";
            string fltr = "";
            //Elegir filtro
            switch (filter)
            {
                case 'p':
                    fltr = filter_passwd;
                    break;
                case 'i':
                    fltr = filter_id;
                    break;
            }
            //Filtrar caracter por caracter
            for (int t = 0; t < text.Length; t++)
            {
                for (int f = 0; f < fltr.Length; f++)
                {
                    if (text.Substring(t, 1).Equals(fltr.Substring(f, 1)))
                    {
                        answ += text.Substring(t, 1); break;
                    }
                }
            }
            return answ;
        }
      
    }
}
