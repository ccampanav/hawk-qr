﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;

namespace HawkQR
{
    public class HQRgenerator
    {
        private string id, passwd;
        private Bitmap template;
        private Color color;
        private Point origin;

        //Establecer la contraseña y plantilla por el usuario. Calcular la llave de crifrado y descifrado
        public HQRgenerator(string user_passwd, Bitmap user_template, Color user_color, Point user_origin)
        {
            passwd = user_passwd; template = user_template;
            color = user_color; origin = user_origin;
            CalcKeyEncode();
            CalcKeyDecode();
        }

        //Generar código HQR de un id
        public Bitmap Generate(string user_id)
        {
            id = user_id; 
            BinarizeID();
            EncodeID();
            GenerateBMP();
            return template;
        }

        //######################### 1.1. LLAVE DE CIFRADO #########################

        //Calcular llave de codificación
        private List<int> key_encode;
        private void CalcKeyEncode()
        {
            //############### Cifrado estático
            //Crear lista del 0-224 que representa el índice del ID binarizado
            key_encode = new List<int>();
            for (int i = 0; i < 225; i++)
            {
                key_encode.Add(i);
            }
            //Invertir lista
            List<int> lst_tmp = new List<int>();
            for (int i = key_encode.Count - 1; i >= 0; i--)
            {
                lst_tmp.Add(key_encode[i]);
            }
            //Separar por índices impares y pares
            List<int> lst_odd = new List<int>();
            List<int> lst_even = new List<int>();
            for (int i = 0; i < lst_tmp.Count; i++)
            {
                if ((i + 1) % 2 == 0)
                {
                    lst_even.Add(lst_tmp[i]);
                }
                else
                {
                    lst_odd.Add(lst_tmp[i]);
                }
            }
            //Volver a concatenar de listas impares y pares
            key_encode.Clear();
            for (int i = 0; i < lst_odd.Count; i++)
            {
                key_encode.Add(lst_odd[i]);
            }
            for (int i = 0; i < lst_even.Count; i++)
            {
                key_encode.Add(lst_even[i]);
            }
            //############### Cifrado dinámico
            //Calcular SUM y MPWD de la contraseña
            int x, sum = 0;
            List<int> pwd = new List<int>(), mpwd = new List<int>();
            for (int i = 0; i < passwd.Length; i++)
            {
                x = int.Parse(passwd.Substring(i, 1));
                pwd.Add(x); sum += x;
            }
            mpwd = new List<int>();
            x = pwd.Count % 3;
            if (x == 1)
            {
                pwd.Add(0); pwd.Add(0);
            }
            else if (x == 2)
            {
                pwd.Add(0);
            }
            x = 0;
            for (int i = 0; i < pwd.Count / 3; i++)
            {
                mpwd.Add(pwd[x] + pwd[x + 1] + pwd[x + 2]);
                x += 3;
            }
            if (x == 1)
            {
                pwd.RemoveAt(pwd.Count - 1); pwd.RemoveAt(pwd.Count - 1);
            }
            else if (x == 2)
            {
                pwd.RemoveAt(pwd.Count - 1);
            }
            //Realizar iteraciones de cifrado dinámico        
            int m, n, ite;
            for (int j = 0; j < mpwd.Count; j++)
            {
                m = mpwd[j]; ite = j + 1;
                //Rotación a la izquierda
                n = sum - m + ite;
                LeftRotation(n);
                //Invertir en grupos
                if (m == 0)
                {
                    n = sum + ite;
                }
                else
                {
                    n = ((sum - (sum % m)) / m) + ite;
                }
                ReverseInGroups(n);
                //Rotación a la derecha
                n = sum - ite + m;
                RightRotation(n);
                //Intercambio
                ExchangeBits(ite, pwd, mpwd, sum);
                //Invertir todo
                FullReverse();
            }
        }

        //Rotar a la izquierda N posiciones
        private void LeftRotation(int n)
        {
            int aux;
            for (int x = 0; x < n; x++)
            {
                aux = key_encode[0];
                key_encode.RemoveAt(0);
                key_encode.Add(aux);
            }
        }

        //Rotar a la derecha N posiciones
        private void RightRotation(int n)
        {
            int aux;
            for (int x = 0; x < n; x++)
            {
                aux = key_encode[key_encode.Count - 1];
                key_encode.RemoveAt(key_encode.Count - 1);
                key_encode.Insert(0, aux);
            }
        }

        //Invertir en grupos de N caracteres
        private void ReverseInGroups(int n)
        {
            int rgi = (key_encode.Count - (key_encode.Count % n)) / n;
            List<int> lst_tmp;
            int limMin = 0, limMax = n - 1, aux;
            for (int k = 0; k < rgi; k++)
            {
                //Guardar caracteres de grupo en temporal
                lst_tmp = new List<int>();
                for (int l = limMax; l >= limMin; l--)
                {
                    lst_tmp.Add(key_encode[l]);
                }
                aux = -1;
                //Escribir caracteres de grupo temporal de manera invertida
                for (int l = limMin; l <= limMax; l++)
                {
                    aux++; key_encode[l] = lst_tmp[aux];
                }
                limMin += n; limMax += n;
            }
        }

        //Intercambio de caracteres (bits)
        private void ExchangeBits(int i, List<int> pwd, List<int> mpwd, int sum)
        {
            List<int> ecp = new List<int>(), ecj = new List<int>();
            //Calcular lista de índices de origen y salto
            for (int p = 0; p < pwd.Count; p++)
            {
                ecp.Add((pwd[p] * i) + sum);
                ecj.Insert(0, (pwd[p] + i) + mpwd[i - 1]);
            }
            int t, ia, ib;
            //Realizar intercambios
            for (int p = 0; p < ecp.Count; p++)
            {
                ia = ecp[p]; ib = ia + ecj[p];
                while (ia >= key_encode.Count)
                {
                    ia -= key_encode.Count;
                }
                while (ib >= key_encode.Count)
                {
                    ib -= key_encode.Count;
                }
                t = key_encode[ia];
                key_encode[ia] = key_encode[ib];
                key_encode[ib] = t;
            }
        }

        //Invertir lista completa
        private void FullReverse()
        {
            List<int> lst_tmp = new List<int>();
            for (int i = key_encode.Count - 1; i >= 0; i--)
            {
                lst_tmp.Add(key_encode[i]);
            }
            key_encode.Clear();
            for (int i = 0; i < lst_tmp.Count; i++)
            {
                key_encode.Add(lst_tmp[i]);
            }
        }

        //######################### 1.2. LLAVE DE DESCIFRADO #########################

        //Obtener llave de decodificación
        private List<int> key_decode;
        private void CalcKeyDecode()
        {
            key_decode = new List<int>();
            //Iterar del 0-224
            for (int i = 0; i < key_encode.Count; i++)
            {
                //Obtener el índice del número deseado
                for (int d = 0; d < key_encode.Count; d++)
                {
                    if (key_encode[d] == i)
                    {
                        key_decode.Add(d); break;
                    }
                }
            }
        }

        //######################### 2. BINARIZACIÓN #########################

        //Binarizar id dado el filtro de caracteres preestablecido
        private IDBinary id_binarized;
        private void BinarizeID()
        {
            //Obtener y binarizar cada caracter del ID
            id_binarized = new IDBinary();
            string chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789.";
            string tmp;
            for (int n = 0; n < id_binarized.segments.Length; n++)
            {
                if (n < id.Length)
                {
                    tmp = id.Substring(n, 1);
                    for (int c = 0; c < chars.Length; c++)
                    {
                        if (tmp.Equals(chars.Substring(c, 1)))
                        {
                            id_binarized.segments[n] = Dec2Bin(c + 1, 6);
                            break;
                        }
                    }
                }
                else
                {
                    id_binarized.segments[n] = Dec2Bin(0, 6);
                }
            }
            //Obtener conteo total de ceros y unos para los 2 validadores
            int sum0 = 0, sum1 = 0;
            foreach (string s in id_binarized.segments)
            {
                for (int c = 0; c < 6; c++)
                {
                    if (s.Substring(c, 1).Equals("0"))
                    {
                        sum0++;
                    }
                    if (s.Substring(c, 1).Equals("1"))
                    {
                        sum1++;
                    }
                }
            }
            id_binarized.chk0 = Dec2Bin(sum0, 8);
            id_binarized.chk1 = Dec2Bin(sum1, 8);
            //Añadir 29 bits aleatorios para los primeros 29 segmentos del ID binarizado
            string bits_random = BitsRandom(29);
            for (int r = 0; r < 29; r++)
            {
                id_binarized.segments[r] += bits_random.Substring(r, 1);
            }
        }

        //Convertir número decimal a binario de N longitud
        private string Dec2Bin(int dec, int n)
        {
            string answ = "", bin_inv = "";
            int aux;
            while (true)
            {
                aux = dec % 2;
                bin_inv += aux;
                dec -= aux;
                dec /= 2;
                if (dec == 0 || dec == 1)
                {
                    break;
                }
            }
            bin_inv += dec;
            for (int i = bin_inv.Length - 1; i >= 0; i--)
            {
                answ += bin_inv.Substring(i, 1);
            }
            //Añadir n ceros a la izquierda. Si n es -1 omitir.
            if (n != -1)
            {
                for (int i = answ.Length; i < n; i++)
                {
                    answ = "0" + answ;
                }
            }
            return answ;
        }

        //Generar N bits aleatorios en cadena
        private string BitsRandom(int n)
        {
            string answ = "";
            Random r;
            while (answ.Length < n)
            {
                r = new Random();
                answ += Dec2Bin(r.Next(100, 8000), -1);
            }
            return answ;
        }

        //######################### 3. CODIFICACIÓN #########################

        //Aplicar llave de codificación al id binarizado
        private string id_encoded;
        private void EncodeID()
        {
            id_encoded = "";
            string id_binarized_str = id_binarized.GetString();
            for (int i = 0; i < key_encode.Count; i++)
            {
                id_encoded += id_binarized_str.Substring(key_encode[i], 1);
            }
        }

        //######################### 3.1. COMPROBACIÓN #########################

        //Decodificar id codificado para comprobar similitud con el id original binarizado
        public bool CheckIntegrity()
        {
            bool answ = false;
            string id_decoded = "";
            string id_binarized_str = id_binarized.GetString();
            //Aplicar llave de decodificación a id codificado
            for (int i = 0; i < key_decode.Count; i++)
            {
                id_decoded += id_encoded.Substring(key_decode[i], 1);
            }
            if (id_decoded.Equals(id_binarized_str) && id_encoded.Length == 225)
            {
                answ = true;
            }
            return answ;
        }

        //######################### 4. GENERACIÓN #########################

        //Generar mapa de bits con el id codificado
        Graphics graphics_hqr;
        private void GenerateBMP()
        {
            int radius_general = 1000, rings = 5, radius_bit = 50;
            //Parámetros para 225 bits de almacenamiento
            int margin_general = 20, margin_spacing = 10, size_image = 2040;
            int radius_aux_a = 153, radius_aux_b = 102, radius_aux_general = 1225;
            int[] rings_radius = {940, 830, 720, 610, 500};
            int[] rings_bits = {59, 52, 45, 38, 31};
            Point hqr_origin = new Point(0, 0), point_aux;
            graphics_hqr = Graphics.FromImage(template);
            //Dibujar circunferencia mayor
            point_aux = CoorsPlane2Image(hqr_origin);
            graphics_hqr.DrawEllipse(new Pen(color, margin_general),
               point_aux.X - radius_general, point_aux.Y - radius_general,
               radius_general * 2, radius_general * 2);
            //Dibujar círculos auxiliares
            point_aux = new Point(radius_aux_general, 0);
            DrawBit(RotatePoint(point_aux, 225), radius_aux_a);
            DrawBit(RotatePoint(point_aux, 315), radius_aux_b);
            //Dibujar bits de los anillos
            int index_id = -1;
            float angle, angle_aux;
            for (int r = 0; r < rings; r++)
            {
                //Calcular grados de paso
                angle = 360f / rings_bits[r];
                //Obtener distancia del origen al anillo en cuestión
                point_aux = new Point(rings_radius[r], 0);
                angle_aux = -angle;
                //Iterar anillo en cuestión
                for (int b = 0; b < rings_bits[r]; b++)
                {
                    angle_aux += angle;
                    index_id++;
                    if (id_encoded.Substring(index_id, 1).Equals("1"))
                    {
                        DrawBit(RotatePoint(point_aux, angle_aux), radius_bit);
                    }                   
                }
            }
        }

        //Dibujar bit en coordenadas de plano cartesiano y con radio X
        private void DrawBit(Point center, int radius)
        {
            Point center_img = CoorsPlane2Image(center);
            graphics_hqr.FillEllipse(new SolidBrush(color),
                center_img.X - radius, center_img.Y - radius,
                radius * 2, radius * 2);
        }

        //Convertir coordenadas de plano cartesiano a plantilla
        private Point CoorsPlane2Image(Point point)
        {
            return new Point(point.X + origin.X, -1 * (point.Y - origin.Y));
        }

        //Rotar un punto N grados
        private Point RotatePoint(Point point, float angle)
        {
            //Convertir ángulo a radianes
            angle = angle * (float)(Math.PI / 180f);
            //Calcular puntos con rotación
            float x = (float)(point.X * Math.Cos(angle) - point.Y * Math.Sin(angle));
            float y = (float)(point.X * Math.Sin(angle) + point.Y * Math.Cos(angle));
            //Retornar puntos redondeados
            return new Point((int)Math.Round(x), (int)Math.Round(y));
        }
    }

    //Clase para almacenar un ID binarizado: 30 segmentos y 2 validadores
    class IDBinary
    {
        public string[] segments;
        public string chk0, chk1;

        public IDBinary()
        {
            segments = new string[30];
            chk0 = ""; chk1 = "";
        }

        //Obtener cadena del objeto
        public string GetString()
        {
            string answ = "";
            for (int s = 0; s < segments.Length; s++)
            {
                answ += segments[s];
            }
            return answ + chk0 + chk1;
        }
    }
}
