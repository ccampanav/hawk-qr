﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace HawkQR
{
    public partial class Generator : Form
    {
        private Home frm_parent;
        private Auxiliar aux;
        private List<Model> models;
        HQRgenerator hqr_generator;

        public Generator(Home fp)
        {
            InitializeComponent();
            frm_parent = fp;
            aux = new Auxiliar();
            chkbx_passwd_CheckedChanged(null, null);
            lbl_models_refresh_Click(null, null);
        }

        //Obtener los modelos de la ruta dada
        private void GetModels()
        {
            //Obtener archivos de la carpeta models
            models = new List<Model>();
            Model model_aux;
            string[] content, name;
            List<string> mdls = aux.GetFiles(Environment.CurrentDirectory + "\\models", "txt");
            for (int m = 0; m < mdls.Count; m++)
            {
                content = aux.GetContentOfFile(Environment.CurrentDirectory + "\\models\\" + mdls[m], 3);
                //El archivo tiene 3 líneas
                if (content != null)
                {
                    name = mdls[m].Split('.');
                    model_aux = new Model(name[0], content[0], content[1], content[2]);
                    if (model_aux.GetIntegrity())
                    {
                        //Obtener solo modelos íntegros
                        models.Add(model_aux);
                    }
                }             
            }
        }

        //Establecer mensaje de estado
        private void SetStatus(string text)
        {
            lbl_status.Text = text;
        }

        //Obtener modelos y cargarlos
        private void lbl_models_refresh_Click(object sender, EventArgs e)
        {
            //Limpiar lista, índice y controles de detalles
            lstbx_models_list.Items.Clear();
            lbl_models_count.Text = "Total: 0";
            lbl_template.Text = "Plantilla: ";
            lbl_origin.Text = "Origen HQR: ";
            pctbx_color.BackColor = Color.White;
            if (aux.DirectoryExist(Environment.CurrentDirectory + "\\models"))
            {
                //Obtener modelos y cargarlos en lista
                GetModels();
                if (models.Count == 0)
                {
                    SetStatus("Error. No hay modelos disponibles");
                }
                else
                {
                    foreach (Model model in models)
                    {
                        lstbx_models_list.Items.Add(model.name);
                    }
                }
                lbl_models_count.Text = "Total: " + models.Count;
                SetStatus("Listo para generar");
            }
            else
            {
                SetStatus("Error. No existe la carpeta: models");
            }
        }

        //Cargar información del modelo seleccionado por el usuairo
        private void lstbx_models_list_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (lstbx_models_list.SelectedIndex >= 0 && lstbx_models_list.SelectedIndex < models.Count)
            {
                lbl_template.Text = "Plantilla: " + models[lstbx_models_list.SelectedIndex].name;
                lbl_origin.Text = "Origen HQR: " + models[lstbx_models_list.SelectedIndex].origin.X + ", " + models[lstbx_models_list.SelectedIndex].origin.Y;
                pctbx_color.BackColor = models[lstbx_models_list.SelectedIndex].color;
                btn_generate.Enabled = true;
            }
        }

        //Filtrar contraseña
        private void txtbx_passwd_TextChanged(object sender, EventArgs e)
        {
            txtbx_passwd.Text = aux.FilterString(txtbx_passwd.Text, 'p');
            if (txtbx_passwd.Text.Length > 0)
            {
                txtbx_passwd.SelectionStart = txtbx_passwd.Text.Length ;
            }         
        }

        //Mostrar/Ocultar contraseña
        private void chkbx_passwd_CheckedChanged(object sender, EventArgs e)
        {
            txtbx_passwd.UseSystemPasswordChar = !txtbx_passwd.UseSystemPasswordChar;
        }

        //Filtrar ID
        private void txtbx_id_TextChanged(object sender, EventArgs e)
        {
            txtbx_id.Text = aux.FilterString(txtbx_id.Text, 'i');
            if (txtbx_id.Text.Length > 0)
            {
                txtbx_id.SelectionStart = txtbx_id.Text.Length;
            }
        }

        //Generar tarjeta con HQR
        private void btn_generate_Click(object sender, EventArgs e)
        {
            hqr_generator = null;
            DateTime time_start = DateTime.MinValue, time_end = DateTime.MinValue;
            //Desactivar controles y limpiar detalles de salida
            EnableControls(false);
            SetStatus(""); pctbx_output.Image = null;
            lbl_output_time.Text = "Tiempo:";
            lbl_output_result.Text = "Integridad correcta:";
            //Comprobar modelo seleccionado
            if (lstbx_models_list.SelectedIndex >= 0 && lstbx_models_list.SelectedIndex < models.Count)
            {
                //Comprobar id y contraseña de longitudes válidas
                if (txtbx_id.Text.Length > 0)
                {
                    if (txtbx_passwd.Text.Length >= 9)
                    {
                        time_start = DateTime.Now;
                        //Realizar generación de HQR
                        SetStatus("Codificando ID... ");
                        hqr_generator = new HQRgenerator(
                            txtbx_passwd.Text,
                            (Bitmap)models[lstbx_models_list.SelectedIndex].template.Clone(),
                            models[lstbx_models_list.SelectedIndex].color,
                            models[lstbx_models_list.SelectedIndex].origin);
                        pctbx_output.Image = hqr_generator.Generate(txtbx_id.Text);
                        pctbx_output.Image.Save("output.png", System.Drawing.Imaging.ImageFormat.Png);
                        SetStatus("Generación finalizada");
                        time_end = DateTime.Now;
                    }
                    else
                    {
                        SetStatus("Error. La contraseña debe tener de 9-30 caracteres");
                    }
                }
                else
                {
                    SetStatus("Error. El ID debe tener entre 1-30 caracteres");
                }
            }
            else
            {
                SetStatus("Error. Selecciona un modelo");
            }
            //Activar controles y mostrar detalles de salida
            EnableControls(true);
            //pctbx_output.Image = null;
            lbl_output_time.Text = "Tiempo: " + time_end.Subtract(time_start).TotalMilliseconds + " ms";
            if (hqr_generator != null)
            {
                if (hqr_generator.CheckIntegrity())
                {
                    lbl_output_result.Text = "Integridad correcta: Si";
                }
                else
                {
                    lbl_output_result.Text = "Integridad correcta: No";
                }
            }           
        }

        //Activar o Desactivar los controles durante el proceso de generación
        private void EnableControls(bool status)
        {
            lstbx_models_list.Enabled = status;
            lbl_models_refresh.Enabled = status;
            txtbx_passwd.Enabled = status;
            chkbx_passwd.Enabled = status;
            txtbx_id.Enabled = status;
            btn_generate.Enabled = status;
        }
    }

    //Clase para almacenar los modelos creados por el usuario
    class Model
    {
        public string name;
        public Bitmap template;
        public Point origin;
        public Color color;
        bool integrity;

        public Model(string n, string t, string o, string c)
        {
            Auxiliar aux = new Auxiliar();
            integrity = false;
            //Cargar nombre
            if (n.Length > 0)
            {
                name = n;
                //Cargar imagen de plantilla
                if (aux.FileExist(Environment.CurrentDirectory + "\\models\\" + t))
                {
                    template = aux.LoadImageFromFile(Environment.CurrentDirectory + "\\models\\" + t);
                    if (template != null)
                    {
                        //Cargar origin de HQR
                        origin = aux.String2Point(o, template.Width, template.Height);
                        if (origin.X >= 0 && origin.Y >= 0)
                        {
                            //Cargar color de HQR
                            color = aux.String2Color(c);
                            if (color != Color.Empty)
                            {
                                integrity = true;
                            }
                        }
                        
                    }
                }
            }
        }

        //Obtener integridad del objeto dada todas sus propiedades correctas
        public bool GetIntegrity()
        {
            return integrity;
        } 
    }
}
